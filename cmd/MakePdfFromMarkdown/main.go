package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"github.com/ericaro/frontmatter"
	"github.com/fatih/structs"
	"github.com/rwestlund/gotex"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"text/template"
)

var (
	err      error
	fileName string
)

// BriefDaten - struct to hold data to fill template
type BriefDaten struct {
	RecipientName    string `yaml:"RecipientName"`
	RecipientStrasse string `yaml:"RecipientStrasse"`
	RecipientPLZ     string `yaml:"RecipientPLZ"`
	RecipientOrt     string `yaml:"RecipientOrt"`
	Subject          string `yaml:"Subject"`
	Opening          string `yaml:"Opening,omitempty"`
	Closing          string `yaml:"Closing,omitempty"`
	Content          string `yaml:"-" fm:"content"`
}

func init() {
	flag.StringVar(&fileName, "f", "", "markdown file to read")
}

func main() {
	flag.Parse()
	if fileName == "" {
		err = fmt.Errorf("no filename given, use '-f <filename>' switch")
		log.Fatal(err)
	}
	// read in markdown
	d, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Fatal(err)
	}
	// unmarshal frontmatter
	v := new(BriefDaten)
	err = frontmatter.Unmarshal(d, v)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("%#v\n", v)
	// make map from struct
	m := structs.Map(v)
	log.Printf("Map: %s", m)
	// load template
	// create a template function map to have custom functions available from within templates
	funcMap := template.FuncMap{
		// function "inc" increases an integer by one,
		// usefull to start counting with 1 in outputs.
		"inc": func(i int) int {
			return i + 1
		},
		// isset - checks if a template variable is set,
		// usefull to change template behaviour on the fact
		// if certain variable are set (or not).
		"isset": func(name string, data interface{}) bool {
			v := reflect.ValueOf(data)
			if v.Kind() == reflect.Ptr {
				v = v.Elem()
			}
			if v.Kind() != reflect.Struct {
				return false
			}
			return v.FieldByName(name).IsValid()
		},
	}
	// load templates from a glob and add functionMap to it
	t := template.Must(template.New("").Funcs(funcMap).ParseGlob("tmpl/tex/Brief/*.tex.tmpl"))
	// execute/fill template
	// create a buffer to store the filled out latex document
	var texbuf bytes.Buffer
	w := bufio.NewWriter(&texbuf)
	// execute template and write output to our buffer
	t.ExecuteTemplate(w, "Brief", m)
	// make sure the buffer is flushed befor we use it.
	w.Flush()
	// write latex document into a file
	err = ioutil.WriteFile("foo.tex", texbuf.Bytes(), 0750)
	if err != nil {
		log.Fatal(err)
	}
	// use gotex to render the PDF from our buffer
	pdf, err := gotex.Render(string(texbuf.Bytes()), gotex.Options{
		//Command: "/usr/bin/pdflatex",
		Command:   "/usr/bin/lualatex",
		Runs:      1,
		Texinputs: "/home/fwa/go/src/github.com/scusi/LatexTemplates/tmpl/tex/Brief/:/home/fwa/go/src/github.com/scusi/LatexTemplates/images/"})

	if err != nil {
		log.Println("render failed ", err)
	}
	// write PDF document
	fmt.Fprintf(os.Stdout, "%s\n", pdf)
}
