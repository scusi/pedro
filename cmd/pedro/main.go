package main

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/ericaro/frontmatter"
	"github.com/fatih/structs"
	"github.com/rwestlund/gotex"
	"github.com/teris-io/cli"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"text/template"
)

var (
	err error
	// Version contains the version information, set during compile time
	Version string
	// Commit contains the commit info, set during compile time
	Commit string
	// Buildtime contains the time the binary was build.
	Buildtime string
	// Branch contains the branch the build was compiled from, set during compiletime
	Branch string
	// path to directory to load templates from
	GlobPath string
)

// BriefDaten - defines meta data for a letter
type BriefDaten struct {
	RecipientName    string `yaml:"RecipientName"`
	Recipient2	 string `yaml:"Recipient2"`
	RecipientStrasse string `yaml:"RecipientStrasse"`
	RecipientPLZ     string `yaml:"RecipientPLZ"`
	RecipientOrt     string `yaml:"RecipientOrt"`
	Subject          string `yaml:"Subject"`
	Signature	 string `yaml:"Signature,omitempty"`
	Opening          string `yaml:"Opening,omitempty"`
	Closing          string `yaml:"Closing,omitempty"`
	Content          string `yaml:"-" fm:"content"`
}

func init() {
	// check if PEDRO_GLOBPATH is set and use it OR fill with default path
	globEnvString, globEnvSet := os.LookupEnv("PEDRO_GLOBPATH") 
	if globEnvSet && len(globEnvString)>0 {
		GlobPath = globEnvString
		log.Printf("GlobPath ('%s') taken from environment.", GlobPath)
	} else {
		GlobPath = "tmpl/tex/Brief/*.tex.tmpl"
		log.Printf("GlobPath ('%s') taken from hard-coded default.", GlobPath)
	}
	// Note: GlobPath will be overwritten from (optional) argument to `pdf` command,
	// see line 165 ff
}

func main() {
	// define the 'new' subcommand
	newmd := cli.NewCommand("new", "creates a new markdown document of given type").
		WithShortcut("n").
		WithArg(cli.NewArg("file", "file to create")).
		//WithOption(cli.NewOption("verbose", "Verbose execution").WithChar('v').WithType(cli.TypeBool)).
		WithOption(cli.NewOption("type", "type of document to create").WithChar('t').WithType(cli.TypeString)).
		WithAction(func(args []string, options map[string]string) int {
			if options["verbose"] == "true" {
				log.Printf("Args: %+v", args)
				log.Printf("Options: %+v", options)
			}
			v := BriefDaten{
				RecipientName:    "Max Mustermann",
				RecipientStrasse: "Hackweg 23",
				RecipientPLZ:     "42235",
				RecipientOrt:     "Hackhausen",
				Subject:          "a small letter",
				Content:          "Der Text muss hier rein",
			}
			var buf bytes.Buffer
			w := bufio.NewWriter(&buf)
			md, err := frontmatter.Marshal(v)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Fprintf(w, "%s", md)
			w.Flush()

			var errorsOccured []error

			for _, f := range args {
				err = ioutil.WriteFile(f, buf.Bytes(), 0700)
				if err != nil {
					aerr := fmt.Errorf("Error writing to '%s': %s", f, err.Error())
					errorsOccured = append(errorsOccured, aerr)
					continue
				}
			}
			if options["verbose"] == "true" {
				fmt.Fprintf(os.Stdout, "%s\n\n", buf.Bytes())
			}
			return 0
		})

	// define the pdf subcommand
	renderPDF := cli.NewCommand("pdf", "renders a PDF from a given markdown file").
		WithArg(cli.NewArg("file", "markdown file to use")).
		WithArg(cli.NewArg("templateGlob", "glob to find the template to use").AsOptional()).
		WithOption(cli.NewOption("no-outfile", "will print output to stdout instead of into a file").WithChar('n').WithType(cli.TypeBool)).
		WithOption(cli.NewOption("dumpTex", "will write generated latex into a file").WithChar('t').WithType(cli.TypeBool)).
		//WithOption(cli.NewOption("templateGlob", "glob to find templates to use").WithChar('tg').WithType(cli.TypeString)).
		WithAction(func(args []string, options map[string]string) int {
			if options["verbose"] == "true" {
				log.Printf("Args: %+v", args)
				log.Printf("Options: %+v", options)
			}
			// prepare pathes for output file
			inputFilePath := filepath.Clean(args[0])                                          // inputFilePath  = "/home/user/Documents/barfoo.md"
			fileName := filepath.Base(inputFilePath)                                          // fileName       = "barfoo.md"
			baseFileName := strings.TrimSuffix(fileName, filepath.Ext(fileName))              // baseFileName   = "barfoo"
			outputFileName := filepath.Join(filepath.Dir(inputFilePath), baseFileName+".pdf") // outputFileName = "/home/user/Documents/barfoo.pdf"
			// read in markdown
			d, err := ioutil.ReadFile(args[0])
			if err != nil {
				log.Fatal(err)
			}
			// unmarshal frontmatter
			v := new(BriefDaten)
			err = frontmatter.Unmarshal(d, v)
			if err != nil {
				log.Fatal(err)
			}
			if options["verbose"] == "true" {
				log.Printf("%#v\n", v)
			}
			// make map from struct
			m := structs.Map(v)
			if options["verbose"] == "true" {
				log.Printf("Map: %s", m)
			}
			// TODO: add a layer to replace variables in the content.
			// load template
			// create a template function map to have custom functions available from within templates
			funcMap := template.FuncMap{
				// function "inc" increases an integer by one,
				// usefull to start counting with 1 in outputs.
				"inc": func(i int) int {
					return i + 1
				},
				// isset - checks if a template variable is set,
				// usefull to change template behaviour on the fact
				// if certain variable are set (or not).
				"isset": func(name string, data interface{}) bool {
					v := reflect.ValueOf(data)
					if v.Kind() == reflect.Ptr {
						v = v.Elem()
					}
					if v.Kind() != reflect.Struct {
						return false
					}
					return v.FieldByName(name).IsValid()
				},
			}
			// check if optional argument GlobPath is set and overwrite GlobPath if set.
			// GlobPath is setup during init phase from environment 
			// variable PEDRO_GLOBPATH, see line 30 and 48 ff
			if len(os.Args[1:])>3 {
				GlobPath = os.Args[4]
				if options["verbose"] == "true" {
					log.Printf("GlobPath ('%s') taken from optional argument.", GlobPath)
				}
			}
			// load templates from a glob and add functionMap to it
			t := template.Must(template.New("").Funcs(funcMap).ParseGlob(GlobPath))
			// execute/fill template
			// create a buffer to store the filled out latex document
			var texbuf bytes.Buffer
			w := bufio.NewWriter(&texbuf)
			// execute template and write output to our buffer
			t.ExecuteTemplate(w, "Brief", m)
			// make sure the buffer is flushed befor we use it.
			w.Flush()
			if options["dumpTex"] == "true" {
				// write latex document into a file
				outputTexFileName := filepath.Join(filepath.Dir(inputFilePath), baseFileName+".tex")
				err = ioutil.WriteFile(outputTexFileName, texbuf.Bytes(), 0750)
				if err != nil {
					log.Fatal(err)
				}
				log.Printf("latex document has been written to: '%s'", outputTexFileName)
			}
			// prepare texinputs pathes for gotex
			basePath, err := os.Getwd()
			if err != nil {
				log.Println(err)
			}
			basePath = filepath.Clean(basePath)
			tmplPath := filepath.Join(basePath, "tmpl", "tex", "Brief")
			imagePath := filepath.Join(basePath, "images")
			texinputs := strings.Join([]string{tmplPath, imagePath, GlobPath}, ":")
			// use gotex to render the PDF from our buffer
			pdf, err := gotex.Render(string(texbuf.Bytes()), gotex.Options{
				//Command: "/usr/bin/pdflatex",
				Command:   "/usr/bin/lualatex",
				Runs:      1,
				Texinputs: texinputs,
			})
			if err != nil {
				log.Println("render failed ", err)
			}

			// write PDF document
			if options["no-outfile"] == "true" {
				fmt.Fprintf(os.Stdout, "%s", pdf)
			} else {
				err = ioutil.WriteFile(outputFileName, []byte(pdf), 0700)
				if err != nil {
					log.Fatal(err)
				}
				log.Printf("output written to '%s'\n", outputFileName)
			}
			return 0
		})

	app := cli.New("pedro").
		WithOption(cli.NewOption("verbose", "Verbose execution").WithChar('v').WithType(cli.TypeBool)).
		WithOption(cli.NewOption("version", "prints version info").WithType(cli.TypeBool)).
		WithCommand(newmd).
		WithCommand(renderPDF).
		WithAction(func(args []string, options map[string]string) int {
			if options["version"] == "true" {
				fmt.Printf("Version: %s, compiled '%s' from branch '%s' @ commit:%s\n", Version, Buildtime, Branch, Commit)
			}
			return 0
		})
	os.Exit(app.Run(os.Args, os.Stdout))
}
