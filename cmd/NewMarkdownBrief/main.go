package main

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/ericaro/frontmatter"
	"log"
	"os"
)

var (
	err  error
	name string
)

func init() {
	//name = os.Args[1]
}

type BriefDaten struct {
	RecipientName    string `yaml:"RecipientName"`
	RecipientStrasse string `yaml:"RecipientStrasse"`
	RecipientPLZ     string `yaml:"RecipientPLZ"`
	RecipientOrt     string `yaml:"RecipientOrt"`
	Subject          string `yaml:"Subject"`
	Opening          string `yaml:"Opening,omitempty"`
	Closing          string `yaml:"Closing,omitempty"`
	Content          string `yaml:"-" fm:"content"`
}

func main() {
	v := BriefDaten{
		RecipientName:    "Max Mustermann",
		RecipientStrasse: "Hackweg 23",
		RecipientPLZ:     "42235",
		RecipientOrt:     "Hackhausen",
		Subject:          "a small letter",
		Content:          "Der Text muss hier rein",
	}
	var buf bytes.Buffer
	w := bufio.NewWriter(&buf)
	md, err := frontmatter.Marshal(v)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(w, "%s", md)
	w.Flush()
	fmt.Fprintf(os.Stdout, "%s\n\n", buf.Bytes())
	/*
		// parse a frontmatter doc
		bd := new(BriefDaten)
		err = frontmatter.Unmarshal(buf.Bytes(), bd)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Fprintf(os.Stdout, "unmarshaled: %+v\n", bd)
		md, err = frontmatter.Marshal(bd)
		if err != nil {
			log.Fatal(err)
		}
		buf.Reset()
		fmt.Fprintf(w, "%s", md)
		w.Flush()
		fmt.Fprintf(os.Stdout, "%s\n\n", buf.Bytes())
	*/
}
