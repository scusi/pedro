## Table of Contents <a name="toc"></a>

* [Table of Contents](#toc)
* [pedro](#pedro)
 * [pedro options](#options)
* [purpose of pedro](#purpose) 
 * [Step1: create markdown template](#step1)
 * [Step2: edit new markdown file](#step2)
 * [Step3: create PDF from markdown](#step3)
 * [pipe PDF outpot directly to viewer](#viewer)
* [How to compile pedro](#compile)
* [How to install pedro](#install)
* [Frontmatter Variables](#fmvar)
* [Environment Variables](#env)

# pedro <a name="pedro"></a>

`pedro` is a small tool to easily create nice letters in PDF format from markdown files.
_pedro_ was inspired by [hugo](https://gohugo.io/).

The following image is a screenshot of a letter, created with pedro.

![Letter created with pedro](images/pedroBrief.png)

## pedro arguments and options <a name="options"></a>
```
pedro [--verbose] [--version]

Description:
    pedro

Options:
    -v, --verbose   Verbose execution
        --version   prints version info

Sub-commands:
    pedro new       creates a new markdown document of given type, shortcut: n
    pedro pdf       renders a PDF from a given markdown file
```

### new - subcommand

`new` does create a new empty markdown document

```
user@host:~/go/src/gitlab.com/scusi/pedro$ pedro new -h
2022/07/21 19:51:04 GlobPath ('/home/user/go/src/gitlab.com/scusi/pedro/tmpl/tex/Brief/*.tex.tmpl') taken from environment.
pedro new [--verbose] [--version] [--type=string] <file>

Description:
    creates a new markdown document of given type

Arguments:
    file            file to create

Options:
    -v, --verbose   Verbose execution
        --version   prints version info
    -t, --type      type of document to create
user@host:~/go/src/gitlab.com/scusi/pedro$ 
```

### pdf - subcommand

`pdf` does generate a new PDF file from the given markdown document
```
user@host:~/go/src/gitlab.com/scusi/pedro$ pedro pdf -h
2022/07/21 19:50:59 GlobPath ('/home/user/go/src/gitlab.com/scusi/pedro/tmpl/tex/Brief/*.tex.tmpl') taken from environment.
pedro pdf [--verbose] [--version] [--no-outfile] [--dumpTex] <file> [templateGlob]

Description:
    renders a PDF from a given markdown file

Arguments:
    file               markdown file to use
    templateGlob       glob to find the template to use, optional

Options:
    -v, --verbose      Verbose execution
        --version      prints version info
    -n, --no-outfile   will print output to stdout instead of into a file
    -t, --dumpTex      will write generated latex into a file
```

## purpose of pedro <a name="purpose"></a>

make nice DIN 5008 letters in PDF format from markdown.

with this software you can create a markdown template file, which you can edit and then create a nice letter as PDF.
the 3 steps neccessary are described below.

### Step 1: create a new markdown template file <a name="step1"></a>

```
pedro new myLetter.md
```

the output of above command, written to `myLetter.md`, does look like the following listing.

```
---
RecipientName: Max Mustermann
RecipientStrasse: Hackweg 23
RecipientPLZ: "42235"
RecipientOrt: Hackhausen
Subject: a small letter
---
Der Text muss hier rein
```

### Step 2: edit markdown template to fit your needs <a name="step2"></a>

go edit the file `myLetter.md` with your favourite editor. Make the file fit your needs.
Everything between the `---` is called  `frontmatter`. Here you set the
metadata for your letter, recipient information, subject, optional an opening
and closing.

After the frontmatter part is the contents of your letter, the actual text.
Delete the default text `Der Text muss hier rein` and write your own contents.

### Step 3: make a PDF from the markdown file <a name="step3"></a>

After you have edited the markdown template you can use the following command to render the PDF from it.

```
pedro pdf myLetter.md
```

The created PDF file, from above example,  will be written to `myLetter.pdf`.

#### pipe PDF to Viewer <a name="viewer"></a>

Instead of directing output to a file, as above you can also directly pipe it to a PDF viewer like `zathura`.

```
pedro pdf --no-outfile ExampleDocuments/foo.md | zathura -

```

`zathura` is a simple document viewer that can also view PDF and takes input
from standard input. You can install it on debian based linux with `sudo apt-get
install zathura`.

## Requirements <a name="req"></a>

On a default ubuntu you need to also install the following packages as requirements.
```
sudo apt install luatex
sudo apt install texlive-lang-german
sudo apt install texlive-fonts-extra
```

## How to compile <a name="compile"></a>

You can use the provided _[Makefile](Makefile)_. Just execute make build.
```
make build
```

### manual compile

_pedro_ has a few compiletime variables and is therefor meant to be compiled like in the following example.

```
go build -ldflags "-X main.Version=v0.0.2 -X main.Commit=c041c7e -X main.Buildtime=2020-06-29T10:38:48Z -X main.Branch=master -w -s" ./cmd/pedro
```

## How to install <a name="install"></a>

### from source

* clone the gitlab repository
* change directory into cloned repository

The provided [Makefile](Makefile) also has a `install` target. So you can just
execute the following command, in order to install pedro on your system.

```
make install
```

### from binary releases

* download the pre-compiled binary, that matches your operating system and architecture, from the [releases page](https://gitlab.com/scusi/pedro/-/releases).
* unpack the downloaded archive
```
tar -vxzf <downloaded-release-file.tar.gz>
```
* copy `pedro` to some directory which is within your PATH environment variable.

## frontmatter variables <a name="fmvar"></a>

pedro does know about the following frontmatter variables:

* RecipientName - First- and Lastname of recipient
* Recipient2 - (optional) a 2nd line for Recipient if needed
* RecipientStrasse - recipient street and housenumber
* RecipientPLZ - recipient postal code
* RecipientOrt - recipient place
* Subject - subject of the letter
* Opening - (optional) a non default opening of the letter 
* Closing - (optional) a non default closing of the letter

## environment variables <a name="env"></a>

* `PEDRO_GLOBPATH` - defines the Globpath where to load templates from.

`PEDRO_GLOBPATH` does overwrite the hard-coded default (which is `tmpl/tex/Brief/*.tex.tmpl`).
The optional argument `GlobPath` in the `pedro pdf` subcommand does overwrite `PEDRO_GLOBPATH`.

### GlobPath Examples

`pedro pdf Letter.md` - would use the hardcoded default unless the environment variable `PEDRO_GLOBPATH` was set before execution.

```
export PEDROGLOBPATH="/home/user/Documents/tmpl/tex/Brief/*tex.tmpl"
pedro pdf Letter.md
```

The above commands would first set the `PEDRO_GLOBPATH` environment variable and then execute pedro pdf subcommand.
In this case the hardcoded default vale would be overwritten with the content of the `PEDRO_GLOBPATH` value.

```
pedro pdf Letter.md /home/otheruser/Documents/tmpl/tex/Brief/*tex.tmpl
```

The above example would overwrite the `PEDRO_GLOBPATH` variable with the given (optional) 3rd argument (`/home/otheruser/Documents/tmpl/tex/Brief/*tex.tmpl`)
