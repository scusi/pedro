module gitlab.com/scusi/pedro

go 1.17

require (
	github.com/ericaro/frontmatter v0.0.0-20200210094738-46863cd917e2
	github.com/fatih/structs v1.1.0
	github.com/rwestlund/gotex v0.0.0-20170412080108-3c68d9bfff3b
	github.com/teris-io/cli v1.0.1
)

require gopkg.in/yaml.v2 v2.3.0 // indirect
