SHELL:=/bin/bash

COMMIT := $(shell git rev-parse --short HEAD)
VERSION := $(shell git describe --abbrev=1 --tags)
BUILDTIME := $(shell date -u '+%Y-%m-%dT%H:%M:%SZ')
BRANCH := $(shell git branch | grep \* | cut -d ' ' -f2)


GOLDFLAGS += -X main.Version=$(VERSION)
GOLDFLAGS += -X main.Commit=$(COMMIT)
GOLDFLAGS += -X main.Buildtime=$(BUILDTIME)
GOLDFLAGS += -X main.Branch=$(BRANCH)
GOLDFLAGS += -w -s
GOFLAGS = -ldflags "$(GOLDFLAGS)"

run: build
	./pedro -h

install: build
	go install $(GOFLAGS) ./cmd/pedro

build:
	go build $(GOFLAGS) ./cmd/pedro


