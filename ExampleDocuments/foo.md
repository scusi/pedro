---
RecipientName: Tobias Mustermann
RecipientStrasse: Hackweg 23
RecipientPLZ: "42235"
RecipientOrt: Hackhausen
Subject: kleiner LaTex Brief 
Opening: Sehr geehrter Herr Mustermann,
Closing: Mit freundlichen Grüßen

---
dies ist ein kleiner Brief, erstellt aus einem markdown dokument mit
frontmatter, unter linux mit golang und lualatex.

Das ist schon richtig cool wie man damit einfach persönliche Briefe schreiben
kann, ganz ohne irgend ein Monster von OfficeSuite.

Ich denke das ist toll.
