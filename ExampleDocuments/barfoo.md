---
RecipientName: Ingeborg Bachmann 
RecipientStrasse: Via Giulia, 66
RecipientPLZ: "00186"
RecipientOrt: Roma RM, Italia
Subject: ein kleiner Brief zur Anerkennung ihrer Verdienste
Opening: Sehr geehrte Frau Bachmann,
Closing: Hochachtungsvoll

---
ich schreibe ihnen diesen Brief um ihnen meine Anerkennung für ihr Lebenswerk zum Ausdruck zu bringen.

Vielen Dank für ihren Beitrag die Welt zu einem besseren Ort zu machen.

